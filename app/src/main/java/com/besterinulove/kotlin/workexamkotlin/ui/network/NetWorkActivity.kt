package com.besterinulove.kotlin.workexamkotlin.ui.network

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.besterinulove.kotlin.workexamkotlin.ui.recycler.PhotoAdapter
import com.besterinulove.kotlin.workexamkotlin.R
import kotlinx.android.synthetic.main.activity_net_work.*

class NetWorkActivity : AppCompatActivity() {
    val TAG = NetWorkActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_net_work)
        val adapter = PhotoAdapter()

        recycler_network.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(this@NetWorkActivity, 4)
            this.adapter = adapter
            setItemViewCacheSize(50)
        }


        val viewModel = ViewModelProviders.of(this).get(NetWorkViewModel::class.java)
        viewModel.getPhotoLiveData().observe(this, Observer {
            adapter.photos = it
            adapter.notifyDataSetChanged()
        })
    }
}
