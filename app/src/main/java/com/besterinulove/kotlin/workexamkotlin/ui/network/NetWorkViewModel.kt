package com.besterinulove.kotlin.workexamkotlin.ui.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.besterinulove.kotlin.workexamkotlin.data.Repository
import com.besterinulove.kotlin.workexamkotlin.data.Photo

class NetWorkViewModel : ViewModel() {
    private val repository: Repository =
        Repository()

    fun getPhotoLiveData():LiveData<List<Photo>>{
        return repository.getPhotos()
    }
}