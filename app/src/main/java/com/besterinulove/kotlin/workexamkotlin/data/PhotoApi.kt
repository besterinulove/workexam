package com.besterinulove.kotlin.workexamkotlin.data

import retrofit2.Call
import retrofit2.http.GET

interface PhotoApi {
    @GET("photos")
    fun getPhotos(): Call<List<Photo>>
}