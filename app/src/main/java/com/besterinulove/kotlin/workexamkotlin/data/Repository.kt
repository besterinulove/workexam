package com.besterinulove.kotlin.workexamkotlin.data

import androidx.lifecycle.LiveData
import com.besterinulove.kotlin.workexamkotlin.ui.network.PhotoLiveData

class Repository {
    private val photoLiveData = PhotoLiveData()
    fun getPhotos(): LiveData<List<Photo>> {
        return photoLiveData
    }
}