package com.besterinulove.kotlin.workexamkotlin.ui.recycler

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Handler
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.besterinulove.kotlin.workexamkotlin.data.Photo
import kotlinx.android.synthetic.main.photo_row.view.*
import java.net.URL

class PhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val handler = Handler()
    val idText = itemView.tv_network_id
    val titleText = itemView.tv_network_title
    val img = itemView.img_network

    fun bindPhoto(photo: Photo) {
        idText.text = photo.id.toString()
        titleText.text = photo.title
        img.tag = photo.thumbnailUrl
        Thread(DownloadImageRunnable(img, photo.thumbnailUrl)).start()
    }

    inner class DownloadImageRunnable(
        val imgView: ImageView,
        val thumbnailUrl: String
    ) : Runnable {
        override fun run() {
            val url = URL(thumbnailUrl)
            val bitmap = BitmapFactory.decodeStream(url.openStream())

            handler.post {
                if (thumbnailUrl == imgView.tag) {
                    imgView.setImageBitmap(bitmap)
                }
            }
        }
    }
}