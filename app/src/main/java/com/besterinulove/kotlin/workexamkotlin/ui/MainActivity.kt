package com.besterinulove.kotlin.workexamkotlin.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.besterinulove.kotlin.workexamkotlin.ui.network.NetWorkActivity
import com.besterinulove.kotlin.workexamkotlin.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_main_next.setOnClickListener {
            startActivity(Intent(this, NetWorkActivity::class.java))
        }
    }
}
